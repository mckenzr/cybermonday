# CyberMonday

All projects related to artifacts supporting Cyber Monday, a collection of writings on data, software and technology along with related how-tos written by Robert McKenzie.

## Name
CyberMonday

## Description
A list of projects that support various articles written for Cyber Monday.


## Support
Please reachout to robertmmck [at] protonmail.com or on Twitter [at] robbiem242


## Contributing
If you have an idea for a contribution to Cyber Monday please reach out to us.

## Authors and acknowledgment
Robbie M

## License
Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the “Software”), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

## Project status
In-progress

